#include "mygl/CompactApp.hpp"
#include "mygl/ShaderProgram.hpp"
#include "mygl/SSBObject.hpp"
#include <Texture.hpp>

#include <iostream>
#include <iomanip>


class EllipticParaboloid {
public:
    EllipticParaboloid(GLfloat a, GLfloat b, GLfloat radius) : _a(a), _b(b), _radius(radius) {}

    glm::vec3 parameters() const {
        return {_a, _b, _radius};
    }

    glm::mat4x4 transform() const {
        return _transform;
    }

    EllipticParaboloid& move(glm::vec3 translation) {
        _transform = glm::translate(_transform, translation);
        return *this;
    }

    static std::pair<std::vector<glm::vec3>, std::vector<glm::mat4x4>>
    unzip(const std::vector<EllipticParaboloid>& objects) {
        std::vector<glm::vec3> params;
        std::vector<glm::mat4x4> transforms;

        size_t n = objects.size();
        params.reserve(n);
        transforms.reserve(n);

        for (const auto& obj : objects) {
            params.push_back(obj.parameters());
            transforms.push_back(obj.transform());
        }

        return {params, transforms};
    }

private:
    GLfloat _a;
    GLfloat _b;
    GLfloat _radius;
    glm::mat4x4 _transform = glm::identity<glm::mat4x4>();
};


class RaytracingApp : public CompactApp {
protected:
    void prepare() override {
        shaders = std::make_shared<ShaderProgram>();
        auto raytracer_shader = Shader::fromFileFormatted(
                GL_COMPUTE_SHADER, "../task1/691Romanov/shaders/raytracer.comp", {rays_per_pixel});
        shaders->attach(raytracer_shader);
        shaders->link();

        initRaytracerShader();
    }

    void drawGUI(double dt) override {
        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiCond_Once);
        if (ImGui::Begin("Elliptic Paraboloids Raytracer", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
        }
        ImGui::End();
    }

    void draw(double dt) override {
        glClear(GL_COLOR_BUFFER_BIT);

        GLsizei width, height;
        raytracer_output->getSize(width, height);

        shaders->use();
        glDispatchCompute(width, height, 1);
        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT | GL_SHADER_STORAGE_BARRIER_BIT);

        retrieve_debug();

//        int display_w, display_h;
//        glfwGetFramebufferSize(window, &display_w, &display_h);
//        glViewport(0, 0, display_w, display_h);
//        glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
    }

    void finalize() override {
        raytracer_output->saveRGBA8_PNG("raytraced.png");
    }

private:
    void deliverModels() {
        auto uniforms = shaders->uniforms();
        uniforms.set("num_models", static_cast<GLuint>(models.size()));

        auto[params, transforms] = EllipticParaboloid::unzip(models);
        uniforms.set("model_parameters", params);
        uniforms.set("model_transforms", transforms);
    }

    void initRaytracerShader() {
        auto[width, height] = getWindowSize();

        auto uniforms = shaders->uniforms();
        uniforms.set("viewbox_height", 40.0f);
        uniforms.set("camera_transform", glm::identity<glm::mat4x4>());
        deliverModels();

        raytracer_output = std::make_shared<Texture>();
        raytracer_output->initStorage2D(1, GL_RGBA8, width, height);

        shaders->use();
        glBindImageTexture(0, raytracer_output->texture(), 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);

        debug_data = SSBObject<glm::vec4>::create();
        debug_data->reassign(width * height * rays_per_pixel, GL_STATIC_READ);
        uniforms.set("debug_out", debug_data);
    }

    void retrieve_debug() {
        auto data = debug_data->read();
        for (auto vec : data) {
            std::cout << std::setprecision(2) << vec.x << ' ' << vec.y << ' ' << vec.z << ' ' << vec.w << "\n";
        }
        std::cout << "------------------" << std::endl;
    }

public:
    std::vector<EllipticParaboloid> models;

private:
    ShaderProgramPtr shaders;
    TexturePtr raytracer_output;

    SSBObjectPtr<glm::vec4> debug_data;

    int rays_per_pixel = 1;
};

int main() {
    RaytracingApp app;

    app.models.push_back(EllipticParaboloid(1.0f, 1.0f, 1e14f));
    app.models.push_back(EllipticParaboloid(2.0f, 2.0f, 1e30f).move(glm::vec3(0, 0, 7.0f)));
    app.models.push_back(EllipticParaboloid(2.0f, 2.0f, 1e30f).move(glm::vec3(0, 0, -7.0f)));

    app.start();

    return 0;
}
