#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>

GLuint _noIndexedModed;
GLsizei _noIndexedModelVertexCount = 0;

GLuint _indexedModed;
GLsizei _indexedModelVertexCount = 0;
GLsizei _indexCount = 0;

void makeModelWithIndexes()
{
	std::vector<float> points =
	{
		-0.5f, 0.5f, 0.0f,        //координаты
		0.0f, 0.0f, 1.0f, 1.0f,   //цвет

		0.5f, 0.5f, 0.0f,
		0.0f, 1.0f, 0.0f, 1.0f,

		0.5f, -0.5f, 0.0f,
		0.0f, 0.0f, 1.0f, 1.0f,

		-0.5f, -0.5f, 0.0f,
		1.0f, 1.0f, 0.0f, 1.0f,
	};

	_indexedModelVertexCount = points.size() / 7;

	std::vector<unsigned short> indices =
	{
		3, 1, 2,
		3, 0, 1,
	};

	_indexCount = indices.size();

	if (USE_DSA) {
		// Заливаем буфер атрибутов на видеокарту.
		GLuint vbo;
		glCreateBuffers(1, &vbo);
		glNamedBufferData(vbo, points.size() * sizeof(float), points.data(), GL_STATIC_DRAW);

		// Загрузим на видеокарту массив индексов.
		GLuint ibo;
		glCreateBuffers(1, &ibo);
		glNamedBufferData(ibo, sizeof(unsigned short) * indices.size(), indices.data(), GL_STATIC_DRAW);

		//Создаем объект VertexArrayObject для хранения настроек полигональной модели
		glCreateVertexArrays(1, &_indexedModed);
		// Настроим формат 0-го вершинного атрибута - координат вершин.
		GLuint attrVertexCoords = 0;
		GLint compSizeVertexCoords = 3; // Количество компонент в исходных данных, описывающих значение атрибута для вершины.
		{
			glEnableVertexArrayAttrib(_indexedModed, attrVertexCoords);
			GLenum componentType = GL_FLOAT; // Тип компонента атрибута.
			GLboolean normalized = GL_FALSE; // Выполнять нормализацию значений компонентов атрибута.
			GLuint attrOffset = 0; // Относительное смещение атрибута в пачке.
			glVertexArrayAttribFormat(_indexedModed, attrVertexCoords, compSizeVertexCoords, componentType, normalized, attrOffset);
		}

		// Настроим формат 1-ого вершинного атрибута - цвета.
		GLuint attrVertexColor = 1;
		GLint compSizeVertexColor = 4; // Количество компонент в исходных данных, описывающих значение атрибута для вершины.
		{
			glEnableVertexArrayAttrib(_indexedModed, attrVertexColor);
			GLenum componentType = GL_FLOAT; // Тип компонента атрибута.
			GLboolean normalized = GL_FALSE; // Выполнять нормализацию значений компонентов атрибута.
			GLuint attrOffset = compSizeVertexCoords * sizeof(float); // Относительное смещение атрибута в пачке.
			glVertexArrayAttribFormat(_indexedModed, attrVertexColor, compSizeVertexColor, componentType, normalized, attrOffset);
		}

		// Ассоциируем атрибуты с точкой привязки.
		GLuint bufferBinding = 0;
		glVertexArrayAttribBinding(_indexedModed, attrVertexCoords, bufferBinding);
		glVertexArrayAttribBinding(_indexedModed, attrVertexColor, bufferBinding);

		// Настроим точку привязки.
		GLintptr bufferOffset = 0; // Смещение в буфере, начиная с которого брать данные.
		GLsizei bufferStride = (compSizeVertexCoords + compSizeVertexColor) * sizeof(float); // Расстояние между значениями элементов в буфере, описанном в точке привязки (размер пачки).
		glVertexArrayVertexBuffer(_indexedModed, bufferBinding, vbo, bufferOffset, bufferStride);

		// Подключим буфер индексов к VAO.
		glVertexArrayElementBuffer(_indexedModed, ibo);
	}
	else {
		//Создаем буфер VertexBufferObject для хранения координат на видеокарте
		GLuint vbo;
		glGenBuffers(1, &vbo);

		//Делаем этот буфер текущим
		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		//Копируем содержимое массива в буфер на видеокарте
		glBufferData(GL_ARRAY_BUFFER, _indexedModelVertexCount * 7 * sizeof(float), points.data(), GL_STATIC_DRAW);

		//=========================================================

		//Создаем ещё один буфер VertexBufferObject для хранения индексов
		unsigned int ibo;
		glGenBuffers(1, &ibo);

		//Делаем этот буфер текущим
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

		//Копируем содержимое массива индексов в буфер на видеокарте
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, _indexCount * sizeof(unsigned short), indices.data(), GL_STATIC_DRAW);

		//=========================================================

		//Создаем объект VertexArrayObject для хранения настроек полигональной модели
		glGenVertexArrays(1, &_indexedModed);

		//Делаем этот объект текущим
		glBindVertexArray(_indexedModed);

		//Делаем буфер с координатами текущим
		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		//Включаем 0й вершинный атрибут - координаты
		glEnableVertexAttribArray(0);

		//Включаем 1й вершинный атрибут - цвета
		glEnableVertexAttribArray(1);

		//Устанавливаем настройки:
		//0й атрибут,
		//3 компоненты типа GL_FLOAT,
		//не нужно нормализовать,
		//28 - значения расположены в массиве впритык,
		//0 - сдвиг от начала
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(float), nullptr);

		//Устанавливаем настройки:
		//1й атрибут,
		//4 компоненты типа GL_FLOAT,
		//не нужно нормализовать,
		//28 - значения расположены в массиве впритык,
		//12 - сдвиг от начала массива
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 7 * sizeof(float),
			reinterpret_cast<void*>(3 * sizeof(float)));

		//Подключаем буфер с индексами
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

		glBindVertexArray(0);
	}
}

/**
Несколько примеров шейдеров
*/
class SampleApplication : public Application
{
public:
   // MeshPtr _cap;

    ShaderProgramPtr _shader;

    GLuint _ubo;

    GLuint uniformBlockBinding = 0;

    void makeScene() override
    {
        Application::makeScene();

		makeModelWithIndexes();

        

		//_cap = makeCube(5);
        //=========================================================
        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>("gl2shaders/scene.vert", "gl2shaders/scene.frag");

        //=========================================================
        //Инициализация Uniform Buffer Object

	    // Выведем размер Uniform block'а.
	    GLint uniformBlockDataSize;
	    glGetActiveUniformBlockiv(_shader->id(), 0, GL_UNIFORM_BLOCK_DATA_SIZE, &uniformBlockDataSize);
	    std::cout << "Uniform block 0 data size = " << uniformBlockDataSize << std::endl;

        if (USE_DSA) {
            glCreateBuffers(1, &_ubo);
            glNamedBufferData(_ubo, uniformBlockDataSize, nullptr, GL_DYNAMIC_DRAW);
        }
        else {
	        glGenBuffers(1, &_ubo);
	        glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
	        glBufferData(GL_UNIFORM_BUFFER, uniformBlockDataSize, nullptr, GL_DYNAMIC_DRAW);
	        glBindBuffer(GL_UNIFORM_BUFFER, 0);
        }
	    // Привязываем буффер к точке привязки Uniform буферов.
        glBindBufferBase(GL_UNIFORM_BUFFER, uniformBlockBinding, _ubo);


        // Получение информации обо всех uniform-переменных шейдерной программы.
	    if (USE_INTERFACE_QUERY) {
		    GLsizei uniformsCount;
		    GLsizei maxNameLength;
		    glGetProgramInterfaceiv(_shader->id(), GL_UNIFORM, GL_ACTIVE_RESOURCES, &uniformsCount);
		    glGetProgramInterfaceiv(_shader->id(), GL_UNIFORM, GL_MAX_NAME_LENGTH, &maxNameLength);
		    std::vector<char> nameBuffer(maxNameLength);

		    std::vector<GLenum> properties = {GL_TYPE, GL_ARRAY_SIZE, GL_OFFSET, GL_BLOCK_INDEX};
		    enum Property {
			    Type,
			    ArraySize,
			    Offset,
			    BlockIndex
		    };
		    for (GLuint uniformIndex = 0; uniformIndex < uniformsCount; uniformIndex++) {
			    std::vector<GLint> params(properties.size());
			    glGetProgramResourceiv(_shader->id(), GL_UNIFORM, uniformIndex, properties.size(), properties.data(),
					    params.size(), nullptr, params.data());
			    GLsizei realNameLength;
			    glGetProgramResourceName(_shader->id(), GL_UNIFORM, uniformIndex, maxNameLength, &realNameLength, nameBuffer.data());

			    std::string uniformName = std::string(nameBuffer.data(), realNameLength);

			    std::cout << "Uniform " << "index = " << uniformIndex << ", name = " << uniformName << ", block = " << params[BlockIndex] << ", offset = " << params[Offset] << ", array size = " << params[ArraySize] << ", type = " << params[Type] << std::endl;
		    }
	    }
	    else {
		    GLsizei uniformsCount;
		    glGetProgramiv(_shader->id(), GL_ACTIVE_UNIFORMS, &uniformsCount);

		    std::vector<GLuint> uniformIndices(uniformsCount);
		    for (int i = 0; i < uniformsCount; i++)
			    uniformIndices[i] = i;
		    std::vector<GLint> uniformBlocks(uniformsCount);
		    std::vector<GLint> uniformNameLengths(uniformsCount);
		    std::vector<GLint> uniformTypes(uniformsCount);
		    glGetActiveUniformsiv(_shader->id(), uniformsCount, uniformIndices.data(), GL_UNIFORM_BLOCK_INDEX, uniformBlocks.data());
		    glGetActiveUniformsiv(_shader->id(), uniformsCount, uniformIndices.data(), GL_UNIFORM_NAME_LENGTH, uniformNameLengths.data());
		    glGetActiveUniformsiv(_shader->id(), uniformsCount, uniformIndices.data(), GL_UNIFORM_TYPE, uniformTypes.data());

		    for (int i = 0; i < uniformsCount; i++) {
			    std::vector<char> name(uniformNameLengths[i]);
			    GLsizei writtenLength;
			    glGetActiveUniformName(_shader->id(), uniformIndices[i], name.size(), &writtenLength, name.data());
			    std::string uniformName = name.data();

			    std::cout << "Uniform " << "index = " << uniformIndices[i] << ", name = " << uniformName << ", block = " << uniformBlocks[i] << ", type = " << uniformTypes[i] << std::endl;
		    }
	    }
    }

    void update() override
    {
        Application::update();

        //Обновляем содержимое Uniform Buffer Object

#if 0
        //Вариант с glMapBuffer
        glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
        GLvoid* p = glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY);
        memcpy(p, &_camera, sizeof(_camera));
        glUnmapBuffer(GL_UNIFORM_BUFFER);
#elif 0
        //Вариант с glBufferSubData
        glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(_camera), &_camera);
#else
        //Вариант для буферов, у которых layout отличается от std140

        //Имена юниформ-переменных
        const char* names[2] =
        {
            "viewMatrix",
            "projectionMatrix"
        };

        GLuint index[2];
        GLint offset[2];

        //Запрашиваем индексы 2х юниформ-переменных
        glGetUniformIndices(_shader->id(), 2, names, index);

        //Зная индексы, запрашиваем сдвиги для 2х юниформ-переменных
        glGetActiveUniformsiv(_shader->id(), 2, index, GL_UNIFORM_OFFSET, offset);

	    // Вывод оффсетов.
	    static bool hasOutputOffset = false;
	    if (!hasOutputOffset) {
		    std::cout << "Offsets: viewMatrix " << offset[0] << ", projMatrix " << offset[1] << std::endl;
		    hasOutputOffset = true;
	    }

        //Устанавливаем значения 2х юниформ-перменных по отдельности
	    if (USE_DSA) {
		    glNamedBufferSubData(_ubo, offset[0], sizeof(_camera.viewMatrix), &_camera.viewMatrix);
		    glNamedBufferSubData(_ubo, offset[1], sizeof(_camera.projMatrix), &_camera.projMatrix);
	    }
	    else {
		    glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
		    glBufferSubData(GL_UNIFORM_BUFFER, offset[0], sizeof(_camera.viewMatrix), &_camera.viewMatrix);
		    glBufferSubData(GL_UNIFORM_BUFFER, offset[1], sizeof(_camera.projMatrix), &_camera.projMatrix);
	    }
#endif
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();


        //Загружаем на видеокарту значения юниформ-переменных
        unsigned int blockIndex = glGetUniformBlockIndex(_shader->id(), "Matrices");
        glUniformBlockBinding(_shader->id(), blockIndex, uniformBlockBinding);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        //_shader->setMat4Uniform("modelMatrix", _cap->modelMatrix());
        //_cap->draw();
		glBindVertexArray(_indexedModed);

		//Рисуем индексную полигональную модель
		glDrawElements(GL_TRIANGLES, _indexCount, GL_UNSIGNED_SHORT, 0); //Рисуем с помощью индексов

    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}