#include <Application.hpp>

#include <iostream>
#include <vector>
#include <numeric>

#include <Common.h>
#include <Texture.hpp>
#include <ShaderProgram.hpp>
#include <QueryObject.h>
#include <Mesh.hpp>

#define _USE_MATH_DEFINES
#include <math.h>


struct shaderData {
    int Y;
    int X;
    int objectCount = 4;
    int rayCount = 1;
    float objects[8 * 4] = {
        0, 0, 0, 50, 1, 0, 0, 1,
        10, 50, -20, 30, 1, 1, 0, 1,
        120, 120, 120, 70, 0, 1, 0, 1,
        20, 30, 40, 50, 0, 0, 1, 1
        // x, y, z, r, red, green, blue, alpha
    };
};

const int WIDTH = 800;
const int HEIGHT = 800;

shaderData data;


class SampleApplication : public Application
{
public:
    //Идентификатор шейдерной программы
    ShaderProgramPtr grayscaleProgram;

    TexturePtr inputTexture;
    TexturePtr outputTexture;

    GLuint _vao;
    GLuint _ebo;
    GLuint _program;

    glm::ivec2 _blockCount;

    void makeScene() override
    {
        Application::makeScene();

        int width = WIDTH;
        int height = HEIGHT;

        outputTexture = std::make_shared<Texture>();
        assert(width % 2 == 0 && height % 2 == 0);
        outputTexture->initStorage2D(1, GL_RGBA8, width, height);

        data.Y = height / 2;
        data.X = width / 2;

        glm::ivec2 gridSize(width, height);
        glm::ivec2 blockSize(8, 4);
        _blockCount = (gridSize + blockSize - 1) / blockSize;

        //Координаты вершин треугольника и далее цвета вершин
        std::vector<float> points =
        {
            // Vertex coords
            0.0f, 0.5f, 0.0f,
            0.5f, -0.5f, 0.0f,
            -0.5f, -0.5f, 0.0f,
            // Colors
            1.0f, 0.0f, 0.0f, 1.0f,
            0.0f, 1.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f, 1.0f,
        };

        GLfloat vertices[] = {
            1.f,  1.f, 0.0f,     1.0f, 1.0f,
            1.f, -1.f, 0.0f,     1.0f, 0.0f,
            -1.f, -1.f, 0.0f,     0.0f, 0.0f,
            -1.f,  1.f, 0.0f,     0.0f, 1.0f
        };
        GLuint indices[] = {
                0, 1, 3,
                1, 2, 3
        };

            //Создаем буфер VertexBufferObject для хранения координат на видеокарте
            GLuint vbo;
            glGenVertexArrays(1, &_vao);
            glGenBuffers(1, &vbo);
            glGenBuffers(1, &_ebo);

            glBindVertexArray(_vao);

            //Делаем этот буфер текущим
            glBindBuffer(GL_ARRAY_BUFFER, vbo);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);      
        
            //Устанавливаем настройки:
            //0й атрибут,
            //3 компоненты типа GL_FLOAT,
            //не нужно нормализовать,
            //0 - значения расположены в массиве впритык,
            //0 - сдвиг от начала
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
            //glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(0));

            //Включаем 0й вершинный атрибут - координаты
            glEnableVertexAttribArray(0);

            //Устанавливаем настройки:
            //1й атрибут,
            //4 компоненты типа GL_FLOAT,
            //не нужно нормализовать,
            //0 - значения расположены в массиве впритык,
            //36 - сдвиг от начала массива
            glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
            //glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(36));

            //Включаем 1й вершинный атрибут - цвета
            glEnableVertexAttribArray(1);



            glBindVertexArray(0);

        //=========================================================

        //Вершинный шейдер
        const char* vertexShaderText =
            "#version 330\n"

            "layout(location = 0) in vec3 vertexPosition;\n"
            "layout(location = 1) in vec2 texCoord;\n"

            "out vec2 TexCoord;\n"

            "void main()\n"
            "{\n"
            "   gl_Position = vec4(vertexPosition, 1.0);\n"
            "   TexCoord = texCoord;\n"
            "}\n";

        //Создаем шейдерный объект
        GLuint vs = glCreateShader(GL_VERTEX_SHADER);

        //Передаем в шейдерный объект текст шейдера
        glShaderSource(vs, 1, &vertexShaderText, nullptr);

        //Компилируем шейдер
        glCompileShader(vs);

        //Проверяем ошибки компиляции
        int status = -1;
        glGetShaderiv(vs, GL_COMPILE_STATUS, &status);
        if (status != GL_TRUE)
        {
            GLint errorLength;
            glGetShaderiv(vs, GL_INFO_LOG_LENGTH, &errorLength);

            std::vector<char> errorMessage;
            errorMessage.resize(errorLength);

            glGetShaderInfoLog(vs, errorLength, 0, errorMessage.data());

            std::cerr << "Failed to compile the shader:\n" << errorMessage.data() << std::endl;

            exit(1);
        }

        //=========================================================

        //Фрагментный шейдер
        const char* fragmentShaderText =
            "#version 330\n"
            "uniform sampler2D ourTexture;\n"
            "in vec2 TexCoord;\n"

            "out vec4 color;\n"

            "void main()\n"
            "{\n"
            "    color =  texture(ourTexture, TexCoord);\n"
            "}\n";

        //Создаем шейдерный объект
        GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);

        //Передаем в шейдерный объект текст шейдера
        glShaderSource(fs, 1, &fragmentShaderText, nullptr);

        //Компилируем шейдер
        glCompileShader(fs);

        //Проверяем ошибки компиляции
        status = -1;
        glGetShaderiv(fs, GL_COMPILE_STATUS, &status);
        if (status != GL_TRUE)
        {
            GLint errorLength;
            glGetShaderiv(fs, GL_INFO_LOG_LENGTH, &errorLength);

            std::vector<char> errorMessage;
            errorMessage.resize(errorLength);

            glGetShaderInfoLog(fs, errorLength, 0, errorMessage.data());

            std::cerr << "Failed to compile the shader:\n" << errorMessage.data() << std::endl;

            exit(1);
        }

        //=========================================================

        //Создаем шейдерную программу
        _program = glCreateProgram();

        //Прикрепляем шейдерные объекты
        glAttachShader(_program, vs);
        glAttachShader(_program, fs);

        //Линкуем программу
        glLinkProgram(_program);

        //Проверяем ошибки линковки
        status = -1;
        glGetProgramiv(_program, GL_LINK_STATUS, &status);
        if (status != GL_TRUE)
        {
            GLint errorLength;
            glGetProgramiv(_program, GL_INFO_LOG_LENGTH, &errorLength);

            std::vector<char> errorMessage;
            errorMessage.resize(errorLength);

            glGetProgramInfoLog(_program, errorLength, 0, errorMessage.data());

            std::cerr << "Failed to link the program:\n" << errorMessage.data() << std::endl;

            exit(1);
        }
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("RayTracing"))
            {
                ImGui::SliderInt("count", &data.rayCount, 1, 10);
            }
        }
        ImGui::End();
    }


    void draw() override {
        int width = WIDTH;
        int height = HEIGHT;
        

        glClear(GL_COLOR_BUFFER_BIT);


        grayscaleProgram = std::make_shared<ShaderProgram>();
        grayscaleProgram->createProgramCompute("gpgpushaders/grayscale_fixedblocksize.comp");
      

        grayscaleProgram->use();

        // Bind textures to binding points so that shader can access them:
        //glBindImageTexture(0, inputTexture->texture(), 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA8);
        glBindImageTexture(0, outputTexture->texture(), 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);

        int paramsLocation = glGetProgramResourceLocation(grayscaleProgram->id(), GL_UNIFORM, "model_parameters");
        glm::ivec2 params = glm::vec3(1, 0, 0);
        if (paramsLocation >= 0)
            glProgramUniform2iv(grayscaleProgram->id(), paramsLocation, (GLsizei)params.length, glm::value_ptr(params));

        // Upload number
        int numberLocation = glGetProgramResourceLocation(grayscaleProgram->id(), GL_UNIFORM, "num_models");
        std::cout << "num_models location: " << numberLocation << std::endl;
        glProgramUniform1i(grayscaleProgram->id(),
            numberLocation,
            10);
        
        grayscaleProgram->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        grayscaleProgram->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        GLuint ssbo;
        glGenBuffers(1, &ssbo);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);
        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(shaderData), reinterpret_cast<const void*>(&data), GL_DYNAMIC_DRAW);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, ssbo);


        // Dispatch compute call for case when block size is set in shader:
        glDispatchCompute(_blockCount.x, _blockCount.y, 1);

        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
        

        //Устанавливаем порт вывода на весь экран (окно)
        glViewport(0, 0, width, height);

        //Очищаем порт вывода (буфер цвета и буфер глубины)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //Рисуем полигональную модель (состоит из треугольников, сдвиг 0, количество вершин 3)
        glBindTexture(GL_TEXTURE_2D, outputTexture->texture());

        glUseProgram(_program);

        //Подключаем VertexArrayObject с настойками полигональной модели
        glBindVertexArray(_vao);

        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        //glDrawArrays(GL_TRIANGLES, 0, 3);

        glBindVertexArray(0);

        //outputTexture->saveRGBA8_PNG("grayscale.png");
    }

};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}