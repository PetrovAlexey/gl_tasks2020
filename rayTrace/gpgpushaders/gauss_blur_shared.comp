#version 430


// Enable extension explicitly to use its capabilities if not implemented in used version
#extension GL_ARB_compute_variable_group_size: enable
// Use variable block size set from dispatch compute call.
layout (local_size_variable) in;

// 'restrict' keyword ensures that different variables access different objects: for possible optimizations.
layout (rgba8, binding=0) uniform restrict readonly image2D inputTex;
layout (rgba8, binding=1) uniform restrict writeonly image2D outputTex;

// Alternative way to provide array for gauss coeffs:
layout (std430) restrict readonly buffer GaussCoeffs
{
    float gaussCoeffs[];
};

uniform ivec2 blockSize; // gl_WorkGroupSize requires explicit compile-time block size specification.
uniform ivec2 blurDirection;
uniform int kernelHalfSize; // [pos - size; pos + size] inclusive

int idot2(ivec2 a, ivec2 b) {
    return a.x*b.x + a.y*b.y;
}

shared vec4 shared_buffer[96];

void main() {
    // This implementation uses strip-like block architecture.
    // Block size = (n+2*w)x1 (or 1x(n+2*w)) where n is output pixels number to process
    // and w is half size of kernel.
    // The implementation benefits from using shared memory: read stripe (n+2*w).

    const ivec2 size = imageSize(inputTex) - 1;

    // Init range start: make it point to first pixel in block shifted to the left by half of kernel size.
    const ivec2 rangeStart = ivec2(gl_WorkGroupID.xy * blockSize.xy) - blurDirection * kernelHalfSize;

    // Now three ways of reading.
    int captureSize = kernelHalfSize * 2 + blockSize.x + blockSize.y - 1;

#if 0
    // First approach: 1 thread reads all
    {
        if (all(equal(gl_LocalInvocationID.xy, ivec2(0, 0)))) {
            ivec2 pos = rangeStart;
            for (int i = 0; i < captureSize; i++) {
                shared_buffer[i] = imageLoad(inputTex, clamp(pos, ivec2(0, 0), size));
                pos += blurDirection;
            }
        }
        barrier();
    }
#elif 0
    // Second approach: each thread reads group of cells
    {
        int nthreads = blockSize.x + blockSize.y - 1;
        int threadID = int(gl_LocalInvocationID.x + gl_LocalInvocationID.y);
        int bulkPerThread = (captureSize + nthreads - 1) / nthreads;
        int bulkStart = bulkPerThread * threadID;
        ivec2 pos = rangeStart + blurDirection * bulkStart;
        for (int i = bulkStart; i < min(bulkStart + bulkPerThread, captureSize); i++) {
            shared_buffer[i] = imageLoad(inputTex, clamp(pos, ivec2(0, 0), size));
            pos += blurDirection;
        }
        barrier();
    }
#else
    // Third approach: threads read data interleaved
    {
        int nthreads = blockSize.x + blockSize.y - 1;
        int threadID = int(gl_LocalInvocationID.x + gl_LocalInvocationID.y);
        ivec2 pos = rangeStart + threadID * blurDirection;
        for (int i = threadID; i < captureSize; i += nthreads) {
            shared_buffer[i] = imageLoad(inputTex, clamp(pos, ivec2(0,0), size));
            pos += blurDirection * nthreads;
        }
        barrier();
    }
#endif

    // Now we finished with common part.

    // Pixel to solve task for:
    const ivec2 position = ivec2(gl_GlobalInvocationID.xy);

    // Benefit from using built-in functions.
    if (any(greaterThan(position, size)))
        return;

    vec4 accum = vec4(0, 0, 0, 0);

#if 0
    // Iterate forwards:
    int pos_buffer = kernelHalfSize + gl_LocalInvocationID.x + gl_LocalInvocationID.y;
    for (int i = 0; i <= kernelHalfSize; i++) {
        accum += shared_buffer[pos_buffer] * gaussCoeffs[i];
        pos_buffer++;
    }

    // Iterate backwards:
    pos_buffer = kernelHalfSize + gl_LocalInvocationID.x + gl_LocalInvocationID.y - 1;
    for (int i = 1; i <= kernelHalfSize; i++) {
        accum += shared_buffer[pos_buffer] * gaussCoeffs[i];
        pos_buffer--;
    }
#else
    // Group by gauss coeff:
    ivec2 poses = ivec2(kernelHalfSize + gl_LocalInvocationID.x + gl_LocalInvocationID.y);
    accum = shared_buffer[poses.x] * gaussCoeffs[0];

    // Iterate in both directions
    for (int i = 1; i <= kernelHalfSize; i++) {
        poses += ivec2(1, -1);
        accum += (shared_buffer[poses.x] + shared_buffer[poses.y]) * gaussCoeffs[i];
    }
#endif

    // Store value:
    imageStore(outputTex, position, accum);
}
